package main

import (
	"fmt"
	"go-fiber-gorm/config"
	"go-fiber-gorm/impl"
	"go-fiber-gorm/model"
	"go-fiber-gorm/repository"
)

func main() {

	fmt.Println("--START--")
	// connection db A
	connectionAUrl := "user:user@tcp(localhost:3306)/DOG_DB"
	db, err := config.MysqlConnection(connectionAUrl)
	if err != nil {
		panic(err)
	}
	repositoryA := repository.NewRepositoryA(db)

	// connection db B
	connectionBUrl := "user:user@tcp(localhost:3307)/CAT_DB"
	db, err = config.MysqlConnection(connectionBUrl)
	if err != nil {
		panic(err)
	}
	repositoryB := repository.NewRepositoryB(db)

	// mock table
	db.AutoMigrate(&model.Dog{})
	db.AutoMigrate(&model.Cat{})

	batch := impl.NewService(repositoryA, repositoryB)
	batch.Process()

	fmt.Println("--STOP--")
}
