package config

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func MysqlConnection(url string) (*gorm.DB, error) {
	var err error

	db, err := gorm.Open(mysql.Open(url), &gorm.Config{
		// SkipDefaultTransaction: true,
		// PrepareStmt:            true,
	})
	if err != nil {
		return nil, err
	}

	return db, nil
}
