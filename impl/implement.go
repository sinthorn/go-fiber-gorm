package impl

import (
	"fmt"
	"go-fiber-gorm/model"
	"go-fiber-gorm/repository"
)

type ServiceIF interface {
	Process()
}

func NewService(repositoryA repository.RepositoryAIF, repositoryB repository.RepositoryBIF) ServiceIF {
	return &Service{
		RepositoryA: repositoryA,
		RepositoryB: repositoryB,
	}
}

type Service struct {
	RepositoryA repository.RepositoryAIF
	RepositoryB repository.RepositoryBIF
}

func (service *Service) Process() {

	// get dog
	d := model.Dog{
		Name: "Captan",
		Age:  20,
	}
	err := service.RepositoryA.SaveDogs(d)
	if err != nil {
		fmt.Println("err save dogs : ", err)
		return
	}

	// save dog
	dogs, err := service.RepositoryA.GetDogs()
	if err != nil {
		fmt.Println("err get dogs : ", err)
		return
	}
	fmt.Println("get dogs : ", dogs)

	// get Cats
	c := model.Cat{
		Name: "Tar",
		Age:  22,
	}
	err = service.RepositoryB.SaveCats(c)
	if err != nil {
		fmt.Println("err save cats : ", err)
		return
	}

	// save cat
	cats, err := service.RepositoryB.GetCats()
	if err != nil {
		fmt.Println("err get cats : ", err)
		return
	}
	fmt.Println("get cats : ", cats)

}
