package model

type Cat struct {
	Name string `gorm:"column:name;primary_key;"`
	Age  int    `gorm:"column:age"`
}

// func (d Dog) TableName() string {
// 	return "Dog" // default table name
// }
