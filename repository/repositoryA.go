package repository

import (
	"errors"
	"go-fiber-gorm/model"

	"gorm.io/gorm"
)

type RepositoryAIF interface {
	GetDogs() ([]model.Dog, error)
	SaveDogs(dogs model.Dog) error
}

func NewRepositoryA(db *gorm.DB) RepositoryAIF {
	return &RepositoryA{
		DB: db,
	}
}

type RepositoryA struct {
	DB *gorm.DB
}

func (repo *RepositoryA) GetDogs() ([]model.Dog, error) {
	var dogs []model.Dog

	repo.DB.Find(&dogs)
	if len(dogs) < 0 {
		return dogs, errors.New("Not found")
	}
	return dogs, nil
}

func (repo *RepositoryA) SaveDogs(dogs model.Dog) error {

	err := repo.DB.Save(&dogs).Error
	return err
}
