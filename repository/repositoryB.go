package repository

import (
	"errors"
	"go-fiber-gorm/model"

	"gorm.io/gorm"
)

type RepositoryBIF interface {
	GetCats() ([]model.Cat, error)
	SaveCats(dogs model.Cat) error
}

func NewRepositoryB(db *gorm.DB) RepositoryBIF {
	return &RepositoryB{
		DB: db,
	}
}

type RepositoryB struct {
	DB *gorm.DB
}

func (repo *RepositoryB) GetCats() ([]model.Cat, error) {
	var cats []model.Cat

	repo.DB.Find(&cats)
	if len(cats) < 0 {
		return cats, errors.New("Not found")
	}
	return cats, nil
}

func (repo *RepositoryB) SaveCats(cats model.Cat) error {

	err := repo.DB.Save(&cats).Error
	return err
}
